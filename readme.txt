readme

 Shell script to read a text file of commands
 and give them for scheduling as a "swarm" of jobs on the Sun Grid Engine scheduler

 Example - ./swarm_scheduler.sh command_file.txt
 command_file.txt has the list of all commands separated by new lines
 
 Author - Soumya Banerjee
 Website - https://sites.google.com/site/neelsoumya/
	    www.cs.unm.edu/~soumya	
 Creation Date - 9th Aug 2013
 License - GNU GPL